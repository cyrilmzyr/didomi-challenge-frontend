import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsentsMenuComponent } from './consents-menu.component';

describe('ConsentsMenuComponent', () => {
  let component: ConsentsMenuComponent;
  let fixture: ComponentFixture<ConsentsMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsentsMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
