import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConsentService {

  consentsList: Consent[] = [{
    name: 'Bojack Horseman',
    email: 'bojack@horseman.com',
    receiveNewsletter: true,
    targetedAds: true,
    visitStatistics: false,
  }, {
    name: 'Princess Carolyn',
    email: 'princess@manager.com',
    receiveNewsletter: true,
    targetedAds: false,
    visitStatistics: false,
  }, {
    name: 'name1',
    email: 'email1',
    receiveNewsletter: true,
    targetedAds: true,
    visitStatistics: true,
  }, {
    name: 'name2',
    email: 'email2',
    receiveNewsletter: false,
    targetedAds: false,
    visitStatistics: false,
  }, {
    name: 'name3',
    email: 'email3',
    receiveNewsletter: true,
    targetedAds: false,
    visitStatistics: true,
  }, {
    name: 'name4',
    email: 'email4',
    receiveNewsletter: false,
    targetedAds: true,
    visitStatistics: false,
  },];

  constructor(private http: HttpClient) { }

  save(consent: Consent): Observable<never> {
    return this.http.post<never>('/consents', consent);
  }

  add(consent: Consent): void {
    this.consentsList.unshift(consent);
  }

  getList(): Observable<Consent[]> {
    return this.http.get<Consent[]>('/consents');
  }
}
