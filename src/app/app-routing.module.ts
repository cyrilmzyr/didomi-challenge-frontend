import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsentsComponent } from './consents/consents.component';
import { GiveConsentComponent } from './give-consent/give-consent.component';

const routes: Routes = [
  {
    path: 'give-consent', component: GiveConsentComponent,
  }, {
    path: 'consents', component: ConsentsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
