interface Consent {
  name: string;
  email: string;
  receiveNewsletter: boolean;
  targetedAds: boolean;
  visitStatistics: boolean;
}
