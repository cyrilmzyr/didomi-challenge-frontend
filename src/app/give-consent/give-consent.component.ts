import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ConsentService } from '../consent.service';

@Component({
  selector: 'app-give-consent',
  templateUrl: './give-consent.component.html',
  styleUrls: ['./give-consent.component.scss']
})
export class GiveConsentComponent implements OnInit {

  consentForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private consentService: ConsentService,
    private router: Router,) { }

  ngOnInit(): void {
    this.consentForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      receiveNewsletter: [false],
      targetedAds: [false],
      visitStatistics: [false],
    });
  }

  get name(): AbstractControl {
    return this.consentForm.get('name');
  }

  get email(): AbstractControl {
    return this.consentForm.get('email');
  }

  get receiveNewsletter(): AbstractControl {
    return this.consentForm.get('receiveNewsletter');
  }

  get targetedAds(): AbstractControl {
    return this.consentForm.get('targetedAds');
  }

  get visitStatistics(): AbstractControl {
    return this.consentForm.get('visitStatistics');
  }

  onSubmitConsentForm(): void {
    const consent: Consent = this.consentForm.value as Consent;
    this.consentService.save(consent)
      .subscribe(() => {
        console.log('consent posted');
      }, () => {
        console.log(`error on posting consent, but that'll do the trick for this time.`);
        this.consentService.add(consent);
        this.router.navigate(['/consents']);
      });
  }
}
