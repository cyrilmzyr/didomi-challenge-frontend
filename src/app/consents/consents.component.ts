import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ConsentService } from '../consent.service';

@Component({
  selector: 'app-consents',
  templateUrl: './consents.component.html',
  styleUrls: ['./consents.component.scss'],
})
export class ConsentsComponent implements OnInit {

  consentsList: MatTableDataSource<Consent>;
  columnsToDisplay = ['name', 'email', 'consent'];

  constructor(private consentService: ConsentService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit(): void {
    this.consentService.getList().subscribe((consentsList: Consent[]) => {
      this.consentsList = new MatTableDataSource<Consent>(consentsList);
      this.consentsList.paginator = this.paginator;
    }, () => {
      console.error(`error on getting consents, but that'll do the trick for this time.`);
      this.consentsList = new MatTableDataSource<Consent>(this.consentService.consentsList);
      this.consentsList.paginator = this.paginator;
    });
  }

}
